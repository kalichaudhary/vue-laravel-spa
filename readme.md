<p align="center">
<img src="https://laravel.com/assets/img/components/logo-laravel.svg"> 
<img src="https://vuejs.org/images/logo.png" height="48"> 
</p>



<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Create a SPA with Vue.JS 2, Vue-Router, Vuex and Laravel 7.8 with browserSync.

simple Single Page Application with the best tools and technologies out there.
Laravel will be used as our Vue.JS 2.5, Vuex 3, VueRouter and many other great libraries will help us achieve our goal.

## Installation
 - `Laravel new vue-spa` [**Note:** direct install Laravel App from Composer too]
 - `npm install --save vuex vue-router validate.js`
 - `npm i`
 - `code .` [**Note:** Open your editor]

## License
 Power by - [kali.com.np](https://kali.com.np)



The Laravel framework 5.8.*  is open-source software licensed under the [MIT license](https://opensource.org/licenses/MIT).
